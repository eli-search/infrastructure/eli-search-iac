tags = {
  Environment = "dev"
  Terraform   = "true"
}

project_account_id = "195848431569"
region             = "eu-west-1"

# S4D-VPC
vpc_id   = "vpc-04687454bd93923a8"
vpc_cidr = "10.178.111.0/25"

# S4D-Private Subnet 1..4
private_subnet_id_list = [
  "subnet-05698563d0262e50c", "subnet-0bd610685b72f95d7", "subnet-04ca3037638fca4d1", "subnet-024eedc5d2a2d8a0b"
]

# eu-west-1a
private_subnet_id_az1_list = ["subnet-05698563d0262e50c", "subnet-024eedc5d2a2d8a0b"]

# eu-west-1b
private_subnet_id_az2_list = ["subnet-04ca3037638fca4d1", "subnet-0bd610685b72f95d7"]

# Terraform
terraform_s3_bucket_name      = "d-ew1-nlex-terraform"
terraform_dynamodb_table_name = "d-ew1-nlex-terraform-locks"

# TED AI project
s3_input_bucket_name      = "d-ew1-nlex-ted-ai-input"  
s3_data_bucket_name       = "d-ew1-nlex-ted-ai-experiments-data"
s3_curated_bucket_name    = "d-ew1-nlex-ted-ai-curated"
s3_ml_data_bucket_name    = "d-ew1-nlex-ted-ai-ml-data"
s3_ml_models_bucket_name  = "d-ew1-nlex-ted-ai-ml-models"
s3_backup_bucket_name     = "d-ew1-nlex-ted-ai-backup"
s3_deployment_bucket_name = "d-ew1-nlex-ted-ai-deployment"
s3_replication_destination_arn = "arn:aws:s3:::d-ew1-nlex-tedai-data-backup"
s3_replication_account_id      = "195848431569"


sagemaker_classifiers_execution_role_name_prefix               = "d-ew1-ted-ai-sagemaker-models-execution"
sagemaker_classifiers_training_role_name_prefix                = "d-ew1-ted-ai-sagemaker-models-training"
sagemaker_classifier_multi_label_division_classifier_name      = "multi-label-division-classifier"
sagemaker_classifier_multi_label_division_classifier_image_url = "dkr.ecr.eu-west-1.amazonaws.com/sagemaker-classifiers:multi-label-division-classifier-v0.0.4"
sagemaker_classifier_multi_label_division_classifier_model_url = "s3://d-ew1-nlex-ted-ai-ml-models/models/multi-label-division-classifier/v0.0.4/model.tar.gz"
sagemaker_classifier_budgetary_value_classifier_name           = "contract-budgetary-values-extractor-classifier"
sagemaker_classifier_budgetary_value_classifier_image_url      = "dkr.ecr.eu-west-1.amazonaws.com/sagemaker-classifiers:contract-budgetary-values-extractor-v0.0.1"
sagemaker_classifier_budgetary_value_classifier_model_url      = "s3://d-ew1-nlex-ted-ai-ml-models/models/contract-budgetary-values-extractor/v0.0.3/model.tar.gz"
sagemaker_classifier_opentender_multi_label_division_classifier_name      = "opentender-multi-label-division-classifier"
sagemaker_classifier_opentender_multi_label_division_classifier_image_url = "dkr.ecr.eu-west-1.amazonaws.com/sagemaker-classifiers:opentender-multi-label-division-classifier-v0.0.1"
sagemaker_classifier_opentender_multi_label_division_classifier_model_url = "s3://d-ew1-nlex-ted-ai-ml-models/models/opentender-multi-label-division-classifier/v0.0.2/model.tar.gz"
sagemaker_classifier_ss_multi_label_cpv_classifier_name                   = "ss-multi-label-cpv-classifier"
sagemaker_classifier_ss_multi_label_cpv_classifier_image_url              = "dkr.ecr.eu-west-1.amazonaws.com/sagemaker-classifiers:ss-multi-label-cpv-classifier-v0.0.3"
sagemaker_classifier_ss_multi_label_cpv_classifier_model_url              = "s3://d-ew1-nlex-ted-ai-ml-models/models/ss-multi-label-cpv-classifier/v0.0.3/model.tar.gz"
sagemaker_classifier_roberta_multi_label_division_classifier_name         = "roberta-multi-label-division-classifier"
sagemaker_classifier_roberta_multi_label_division_classifier_image_url    = "dkr.ecr.eu-west-1.amazonaws.com/sagemaker-classifiers:roberta-multi-label-division-classifier-v0.0.3"
sagemaker_classifier_roberta_multi_label_division_classifier_model_url    = "s3://d-ew1-nlex-ted-ai-ml-models/models/roberta-multi-label-division-classifier/v0.0.1/model.tar.gz"


ecr_repository_application_name           = "ted-applications"
ecr_repository_sagemaker_classifiers_name = "sagemaker-classifiers"

applications_dashboard_repository_url = "dkr.ecr.eu-west-1.amazonaws.com/ted-applications:dashboard-v0.0.7"
dashboard_port                        = 7860

# RDS DB
db_identifier                          = "tedaidb"
db_name                                = "tedaidb"
db_instance_class                      = "db.t3.micro"
db_username                            = "dbadmin"
db_password_secretsmanager_secret_name = "rds-db-credentials"

# SSM path
ssm_prefix                                                   = "/tedai"
ssm_buckets_input_id                                         = "/tedai/s3/input_bucket/id"
ssm_buckets_curated_id                                       = "/tedai/s3/curated_bucket/id"
ssm_buckets_ml_data_id                                       = "/tedai/s3/ml_data_bucket/id"
ssm_buckets_backup_id                                        = "/tedai/s3/backup_bucket/id"
ssm_buckets_deployment_id                                    = "/tedai/s3/deployment_bucket/id"
ssm_classifier_endpoint_multi_label_division_classifier_name = "/tedai/sagemaker/endpoint/multi_label_division_classifier/name"
ssm_classifier_endpoint_budgetary_value_classifier_name      = "/tedai/sagemaker/endpoint/budgetary_value_classifier/name"
ssm_classifier_endpoint_opentender_multi_label_division_classifier_name = "/tedai/sagemaker/endpoint/opentender_multi_label_division_classifier/name"
ssm_classifier_endpoint_ss_multi_label_cpv_classifier_name              = "/tedai/sagemaker/endpoint/ss_multi_label_cpv_classifier/name"
ssm_classifier_endpoint_roberta_multi_label_division_classifier_name    = "/tedai/sagemaker/endpoint/roberta_multi_label_division_classifier/name"
ssm_s3_input_bucket_readwrite_policy_arn                     = "/tedai/iam/policies/s3_input_bucket_readwrite_policy/arn"
ssm_s3_curated_bucket_readwrite_policy_arn                   = "/tedai/iam/policies/s3_curated_bucket_readwrite_policy/arn"
ssm_s3_curated_bucket_readonly_policy_arn                    = "/tedai/iam/policies/s3_curated_bucket_readonly_policy/arn"
ssm_s3_backup_bucket_readonly_policy_arn                     = "/tedai/iam/policies/s3_backup_bucket_readonly_policy/arn"
ssm_s3_deployment_bucket_readwrite_policy_arn                = "/tedai/iam/policies/s3_deployment_bucket_readwrite_policy/arn"
ssm_cloudwatch_logs_write_policy_arn                         = "/tedai/iam/policies/ssm_cloudwatch_logs_write_policy/arn"
ssm_ecs_ingest_resources_task_definition                     = "/tedai/ecs/ingest_resources/task_definition"
ssm_ecs_ingest_resources_cluster_arn                         = "/tedai/ecs/ingest_resources/cluster_arn"
ssm_ecs_ingest_resources_container_name                      = "/tedai/ecs/ingest_resources/container_name"
ssm_ecs_ingest_resources_security_group_id                   = "/tedai/ecs/ingest_resources/security_group_id"
ssm_ecs_notice_data_extraction_task_definition               = "/tedai/ecs/notice_data_extraction/task_definition"
ssm_ecs_notice_data_extraction_cluster_arn                   = "/tedai/ecs/notice_data_extraction/cluster_arn"
ssm_ecs_notice_data_extraction_container_name                = "/tedai/ecs/notice_data_extraction/container_name"
ssm_ecs_notice_data_extraction_security_group_id             = "/tedai/ecs/notice_data_extraction/security_group_id"


# Glue
glue_database_name = "ted-ai"

# IAM
iam_role_prefix   = "D_EW1_TED_AI"
iam_policy_prefix = "D_EW1_TED_AI"

# Lawfulness
lawfulness_resource_prefix = "d-ew1-ted-ai-lawfulness"
lawfulness_task_image      = "dkr.ecr.eu-west-1.amazonaws.com/ted-applications:lawfulness-v0.2.0"
lawfulness_api_port        = 8080
lawfulness_thread_count    = 1
lawfulness_cpu_per_thread           = 4
lawfulness_batch_count              = 1
lawfulness_scheduler_count          = 1
lawfulness_task_count_per_scheduler = 4
lawfulness_task_cron                = "cron(0 0 * * ? *)" # run every day at 00:00

# Ingestion
ingestion_resource_prefix      = "d-ew1-ted-ai-ingestion"
notice_ingestion_resources_url = "dkr.ecr.eu-west-1.amazonaws.com/ted-applications:ingestion-v0.0.2"

# Notice data extraction
notice_data_extraction_prefix        = "d-ew1-ted-ai-notice-data-extraction"
notice_data_extraction_resources_url = "dkr.ecr.eu-west-1.amazonaws.com/ted-applications:notice_data_extraction-v0.0.1"
