##
# Define variables for AWS Bootstrap Module
##

variable "terraform_s3_bucket_name" {
  type = string
}
variable "terraform_dynamodb_table_name" {
  type = string
}
