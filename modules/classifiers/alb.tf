/*resource "aws_security_group" "lb" {
  name        = "ecs-dashboard-load-balancer-security-group"
  description = "controls access to the ALB"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}*/

# Traffic to the ECS cluster should only come from the ALB
/*resource "aws_security_group" "ecs_tasks" {
  name        = "ecs-dashboard-tasks-security-group-alb"
  description = "allow inbound access from the ALB only"
  vpc_id      = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = var.dashboard_port
    to_port         = var.dashboard_port
    //security_groups = [aws_security_group.lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}*/

/*resource "random_shuffle" "az1" {
  input        = var.private_subnet_id_az1_list
  result_count = 1
}*/

/*resource "random_shuffle" "az2" {
  input        = var.private_subnet_id_az2_list
  result_count = 1
}*/

/*resource "aws_alb" "dashboard" {
  name            = "ecs-dashboard-load-balancer"
  subnets         = concat(random_shuffle.az1.result, random_shuffle.az2.result)
  //security_groups = [aws_security_group.lb.id]
  internal        = true
}*/

/*resource "aws_alb_target_group" "dashboard" {
  name        = "ecs-dashboard-target-group"
  port        = var.dashboard_port
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "3"
  }
}*/

# Redirect all traffic from the ALB to the target group
/*resource "aws_alb_listener" "dashboard" {
  load_balancer_arn = aws_alb.dashboard.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.dashboard.id
    type             = "forward"
  }
}*/
