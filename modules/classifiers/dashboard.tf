/*resource "aws_cloudwatch_log_group" "dashboard" {
  name = "/tedai/dashboard"
}

resource "aws_ecs_cluster" "dashboard" {
  name = "dashboard"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  configuration {
    execute_command_configuration {
      logging = "OVERRIDE"
      log_configuration {
        cloud_watch_log_group_name = aws_cloudwatch_log_group.dashboard.name
      }
    }
  }
}

resource "aws_ecs_cluster_capacity_providers" "dashboard" {
  cluster_name = aws_ecs_cluster.dashboard.name

  capacity_providers = ["FARGATE"]
  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

data "aws_iam_policy_document" "dashboard" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "dashboard" {
  name = "${var.iam_policy_prefix}_DASHBOARD_POLICY"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["ssm:GetParameter", "ssm:GetParameters"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "sagemaker:InvokeEndpoint", "sagemaker:CreateEndpoint", "sagemaker:DeleteEndpoint",
          "sagemaker:DescribeEndpoint"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["logs:CreateLogStream", "logs:PutLogEvents"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["ecr:BatchGet*", "ecr:BatchCheck*", "ecr:Get*", "ecr:List*", "ecr:Describe*"]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role" "dashboard" {
  name                 = "${var.iam_role_prefix}_DASHBOARD_ROLE"
  assume_role_policy   = data.aws_iam_policy_document.dashboard.json
  managed_policy_arns  = [aws_iam_policy.dashboard.arn]
  permissions_boundary = "arn:aws:iam::${var.project_account_id}:policy/Team_Admin_Boundary"
}

resource "aws_ecs_task_definition" "dashboard" {
  family                   = "dashboard"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn       = aws_iam_role.dashboard.arn
  task_role_arn            = aws_iam_role.dashboard.arn

  container_definitions = jsonencode([
    {
      name               = "dashboard"
      image              = "${var.project_account_id}.${var.applications_dashboard_docker_image_url}"
      cpu                = 1024
      memory             = 2048
      execution_role_arn = aws_iam_role.dashboard.arn
      task_role_arn      = aws_iam_role.dashboard.arn
      essential          = true
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = "/tedai/dashboard"
          awslogs-region        = "eu-west-1"
          awslogs-stream-prefix = "ecs"
        }
      }
      portMappings = [
        {
          containerPort = var.dashboard_port
          hostPort      = var.dashboard_port
        }
      ]
      healthCheck = {
        command     = ["CMD-SHELL", format("curl -f http://localhost:%s/ || exit 1", var.dashboard_port)]
        startPeriod = 10
      }
      environment = [
        {
          name  = "LAWFULNESS_API_URL"
          value = "http://${var.lawfulness_host}"
        }
      ]
    }
  ])
}*/

/*resource "aws_ecs_service" "dashboard" {
  name            = "dashboard"
  cluster         = aws_ecs_cluster.dashboard.id
  task_definition = aws_ecs_task_definition.dashboard.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = var.private_subnet_id_list
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.dashboard.id
    container_name   = "dashboard"
    container_port   = var.dashboard_port
  }
  depends_on = [aws_iam_role.dashboard, aws_alb.dashboard]
}*/
