/*data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["sagemaker.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "sagemaker_classifiers_policy" {
  name = "${var.sagemaker_classifiers_execution_role_name_prefix}-policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["s3:ListAllMyBuckets", "s3:ListBucket", "s3:HeadBucket"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["s3:Get*"]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::d-ew1-ted-ai-ml-models/*"
      },
      {
        Action   = ["ecr:BatchGet*", "ecr:BatchCheck*", "ecr:Get*", "ecr:List*", "ecr:Describe*"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["cloudwatch:*"]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role" "sagemaker_classifiers_execution_role" {
  name                 = "${var.sagemaker_classifiers_execution_role_name_prefix}-role"
  assume_role_policy   = data.aws_iam_policy_document.instance_assume_role_policy.json
  managed_policy_arns  = [aws_iam_policy.sagemaker_classifiers_policy.arn]
  permissions_boundary = "arn:aws:iam::${var.project_account_id}:policy/Team_Admin_Boundary"
}

# Multi label division classifier
resource "aws_sagemaker_model" "multi_label_division_classifier" {
  name               = var.sagemaker_classifier_multi_label_division_classifier_name
  execution_role_arn = aws_iam_role.sagemaker_classifiers_execution_role.arn

  container {
    container_hostname = var.sagemaker_classifier_multi_label_division_classifier_name
    image              = "${var.project_account_id}.${var.sagemaker_classifier_multi_label_division_classifier_image_url}"
    mode               = "SingleModel"
    model_data_url     = var.sagemaker_classifier_multi_label_division_classifier_model_url
  }

  tags = var.tags
}

resource "aws_sagemaker_endpoint_configuration" "multi_label_division_classifier" {
  name = "${var.sagemaker_classifier_multi_label_division_classifier_name}-endpoint-config"

  production_variants {
    variant_name           = "variant-name"
    model_name             = aws_sagemaker_model.multi_label_division_classifier.name
    initial_instance_count = 1
    instance_type          = "ml.m5.xlarge"
  }

  tags = var.tags
}*/

/*resource "aws_sagemaker_endpoint" "multi_label_division_classifier" {
  name                 = "${var.sagemaker_classifier_multi_label_division_classifier_name}-endpoint"
  endpoint_config_name = aws_sagemaker_endpoint_configuration.multi_label_division_classifier.name

  tags = var.tags
}*/

/*resource "aws_ssm_parameter" "multi_label_division_classifier" {
  name  = var.ssm_classifier_endpoint_multi_label_division_classifier_name
  type  = "String"
  value = aws_sagemaker_endpoint.multi_label_division_classifier.name
}*/


# Opentender Multi label division classifier
/*resource "aws_sagemaker_model" "opentender_multi_label_division_classifier" {
  name               = var.sagemaker_classifier_opentender_multi_label_division_classifier_name
  execution_role_arn = aws_iam_role.sagemaker_classifiers_execution_role.arn

  container {
    container_hostname = var.sagemaker_classifier_opentender_multi_label_division_classifier_name
    image              = "${var.project_account_id}.${var.sagemaker_classifier_opentender_multi_label_division_classifier_image_url}"
    mode               = "SingleModel"
    model_data_url     = var.sagemaker_classifier_opentender_multi_label_division_classifier_model_url
  }

  tags = var.tags
}

resource "aws_sagemaker_endpoint_configuration" "opentender_multi_label_division_classifier" {
  name = "${var.sagemaker_classifier_opentender_multi_label_division_classifier_name}-endpoint-config"

  production_variants {
    variant_name           = "variant-name"
    model_name             = aws_sagemaker_model.opentender_multi_label_division_classifier.name
    initial_instance_count = 1
    instance_type          = "ml.m5.xlarge"
  }

  tags = var.tags
}*/

/*resource "aws_sagemaker_endpoint" "opentender_multi_label_division_classifier" {
  name                 = "${var.sagemaker_classifier_opentender_multi_label_division_classifier_name}-endpoint"
  endpoint_config_name = aws_sagemaker_endpoint_configuration.opentender_multi_label_division_classifier.name

  tags = var.tags
}*/

/*resource "aws_ssm_parameter" "opentender_multi_label_division_classifier" {
  name  = var.ssm_classifier_endpoint_opentender_multi_label_division_classifier_name
  type  = "String"
  value = aws_sagemaker_endpoint.opentender_multi_label_division_classifier.name
}*/

# Budgetary values classifier
/*resource "aws_sagemaker_model" "budgetary_value_classifier" {
  name               = var.sagemaker_classifier_budgetary_value_classifier_name
  execution_role_arn = aws_iam_role.sagemaker_classifiers_execution_role.arn

  container {
    container_hostname = var.sagemaker_classifier_budgetary_value_classifier_name
    image              = "${var.project_account_id}.${var.sagemaker_classifier_budgetary_value_classifier_image_url}"
    mode               = "SingleModel"
    model_data_url     = var.sagemaker_classifier_budgetary_value_classifier_model_url
  }

  tags = var.tags
}

resource "aws_sagemaker_endpoint_configuration" "budgetary_value_classifier" {
  name = "${var.sagemaker_classifier_budgetary_value_classifier_name}-endpoint-config"

  production_variants {
    variant_name           = "variant-name"
    model_name             = aws_sagemaker_model.budgetary_value_classifier.name
    initial_instance_count = 1
    instance_type          = "ml.m5.xlarge"
  }

  tags = var.tags
}*/

/*resource "aws_sagemaker_endpoint" "budgetary_value_classifier" {
  name                 = "${var.sagemaker_classifier_budgetary_value_classifier_name}-endpoint"
  endpoint_config_name = aws_sagemaker_endpoint_configuration.budgetary_value_classifier.name

  tags = var.tags
}*/

/*resource "aws_ssm_parameter" "budgetary_value_classifier" {
  name  = var.ssm_classifier_endpoint_budgetary_value_classifier_name
  type  = "String"
  //value = aws_sagemaker_endpoint.budgetary_value_classifier.name
}*/
