/*resource "aws_iam_policy" "tedai_cloudwatch_logs_write" {
  name        = "${var.iam_policy_prefix}_CLOUDWATCH_LOGS_WRITE_POLICY"
  description = "Write access to CloudWatch logs by application"
  policy      = data.aws_iam_policy_document.tedai_cloudwatch_logs_write.json
}

data "aws_iam_policy_document" "tedai_cloudwatch_logs_write" {
  statement {
    sid = "CloudwatchLogsWrite"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = ["arn:aws:logs:${var.region}:${var.account_id}:log-group:*"]
  }
}

resource "aws_ssm_parameter" "ssm_cloudwatch_logs_write_policy" {
  name  = var.ssm_cloudwatch_logs_write_policy_arn
  type  = "String"
  value = aws_iam_policy.tedai_cloudwatch_logs_write.arn
}*/