variable "account_id" {
  type = string
}

variable "region" {
  type = string
}

variable "iam_policy_prefix" {
  type = string
}

variable "ssm_cloudwatch_logs_write_policy_arn" {
  type = string
}