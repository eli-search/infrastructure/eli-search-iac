output "host" {
  value = aws_db_instance.db_instance.address
}

output "port" {
  value = aws_db_instance.db_instance.port
}

output "password_ssm_parameter_name" {
  value = aws_ssm_parameter.rds_master_password.name
}

output "password_ssm_parameter_arn" {
  value = aws_ssm_parameter.rds_master_password.arn
}
