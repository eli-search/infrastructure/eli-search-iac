/*resource "random_password" "master_password" {
  length  = 30
  special = false
  numeric = true
}

resource "aws_security_group" "security_group" {
  name        = "db-sg"
  description = "Allow connection to the Database"
  vpc_id      = var.vpc_id
}

# Allows the inbound traffic
resource "aws_security_group_rule" "allow_inbound_traffic" {
  description       = "Allow access from VPC"
  type              = "ingress"
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  cidr_blocks       = [var.vpc_cidr]
  security_group_id = aws_security_group.security_group.id
}

# Allows the outbound traffic
resource "aws_security_group_rule" "allow_all_outbound_traffic" {
  description       = "Allow egress access"
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
}

resource "aws_db_subnet_group" "db" {
  subnet_ids  = var.private_subnet_id_list
  description = "database-subnet-group"
}

resource "aws_db_instance" "db_instance" {
  identifier             = var.db_identifier
  db_name                = var.db_name
  instance_class         = var.db_instance_class
  allocated_storage      = 5
  engine                 = "postgres"
  engine_version         = "15.3"
  skip_final_snapshot    = true
  publicly_accessible    = false
  vpc_security_group_ids = [aws_security_group.security_group.id]
  username               = var.db_username
  password               = random_password.master_password.result
  db_subnet_group_name   = aws_db_subnet_group.db.name
}

resource "aws_ssm_parameter" "rds_master_user" {
  name        = "${var.ssm_prefix}/database/master_user"
  type        = "String"
  value       = var.db_username
  description = "The user to connect on the RDS as master"
}

resource "aws_ssm_parameter" "rds_master_password" {
  name        = "${var.ssm_prefix}/database/master_password"
  type        = "SecureString"
  value       = random_password.master_password.result
  description = "The password of the master used of the RDS"
}

resource "aws_ssm_parameter" "rds_endpoint" {
  name        = "${var.ssm_prefix}/database/endpoint"
  type        = "String"
  value       = aws_db_instance.db_instance.endpoint
  description = "The endpoint of the RDS"
}

resource "aws_ssm_parameter" "rds_db" {
  name        = "${var.ssm_prefix}/database/db_name"
  type        = "String"
  value       = aws_db_instance.db_instance.db_name
  description = "The name of the database in the RDS"
}*/
