variable "tags" {
  type = map(string)
}

variable "account_id" {
  type = string
}

variable "region" {
  type = string
}

variable "glue_database_name" {
  type = string
}

variable "s3_bucket_map" {
  type = map(object({ arn = string, name = string }))
}

variable "s3_policies_map" {
  type = map(object({ arn = string, name = string }))
}

variable "iam_role_prefix" {
  type = string
}

variable "iam_policy_prefix" {
  type = string
}