/*resource "aws_cloudwatch_log_group" "cluster" {
  name = "/tedai/ingest-resources"
}

resource "aws_ecs_cluster" "cluster" {
  name = var.resource_prefix

  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  configuration {
    execute_command_configuration {
      logging = "OVERRIDE"
      log_configuration {
        cloud_watch_log_group_name = aws_cloudwatch_log_group.cluster.name
      }
    }
  }
}

resource "aws_ecs_cluster_capacity_providers" "cluster" {
  cluster_name = aws_ecs_cluster.cluster.name

  capacity_providers = ["FARGATE"]
  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

data "aws_iam_policy_document" "ecs_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "iam_policy" {
  name = "${var.iam_policy_prefix}_INGEST_RESOURCES_POLICY"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["sqs:SendMessageBatch", "sqs:SendMessage"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = ["dynamodb:GetItem", "dynamodb:PutItem", "dynamodb:DeleteItem",
        "dynamodb:Scan", "dynamodb:UpdateItem", "dynamodb:DescribeTable", "dynamodb:BatchWrite*"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["logs:CreateLogStream", "logs:PutLogEvents"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["ecr:BatchGet*", "ecr:BatchCheck*", "ecr:Get*", "ecr:List*", "ecr:Describe*"]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role" "iam_role" {
  name                 = "${var.iam_role_prefix}_INGEST_RESOURCES_ROLE"
  assume_role_policy   = data.aws_iam_policy_document.ecs_assume_role_policy.json
  managed_policy_arns  = [aws_iam_policy.iam_policy.arn]
  permissions_boundary = "arn:aws:iam::${var.project_account_id}:policy/Team_Admin_Boundary"
}

resource "aws_ecs_task_definition" "processing_task" {
  family                   = var.resource_prefix
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn       = aws_iam_role.iam_role.arn
  task_role_arn            = aws_iam_role.iam_role.arn

  container_definitions = jsonencode([
    {
      name               = var.resource_prefix
      image              = "${var.project_account_id}.${var.application_ingest_resources_url}"
      cpu                = 1024
      memory             = 2048
      execution_role_arn = aws_iam_role.iam_role.arn
      task_role_arn      = aws_iam_role.iam_role.arn
      essential          = true
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.cluster.name
          awslogs-region        = var.region
          awslogs-stream-prefix = "ecs"
        }
      }
    }
  ])
}*/

/*resource "aws_ecs_service" "ecs_service" {
  name            = var.resource_prefix
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.processing_task.arn
  desired_count   = 0
  launch_type     = "FARGATE"

  network_configuration {
    //security_groups  = [aws_security_group.security_group.id]
    subnets          = var.private_subnet_id_list
    assign_public_ip = false
  }

  depends_on = [aws_iam_role.iam_role]
}*/

/*resource "aws_security_group" "security_group" {
  name        = var.resource_prefix
  description = "Allow internet access in ingest resources task"
  vpc_id      = var.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}*/

/*resource "aws_ssm_parameter" "ingest_resources_task_definition" {
  name  = var.ssm_ecs_ingest_resources_task_definition
  type  = "String"
  value = replace(aws_ecs_service.ecs_service.task_definition, "/:\\d+$/", "")
}*/

/*resource "aws_ssm_parameter" "ingest_resources_cluster_arn" {
  name  = var.ssm_ecs_ingest_resources_cluster_arn
  type  = "String"
  value = aws_ecs_cluster.cluster.arn
}

resource "aws_ssm_parameter" "ingest_resources_container_name" {
  name  = var.ssm_ecs_ingest_resources_container_name
  type  = "String"
  value = var.resource_prefix
}

resource "aws_ssm_parameter" "ingest_resources_security_group_id" {
  name  = var.ssm_ecs_ingest_resources_security_group_id
  type  = "String"
  value = aws_security_group.security_group.id
}*/
