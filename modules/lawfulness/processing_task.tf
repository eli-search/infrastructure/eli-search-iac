/*resource "aws_iam_policy" "processing_task" {
  name = "${var.iam_policy_prefix}_LAWFULNESS_PROCESSING_TASK_POLICY"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["sqs:ReceiveMessage", "sqs:DeleteMessage"]
        Effect   = "Allow"
        Resource = aws_sqs_queue.new_notices.arn
      },
      {
        Action   = ["s3:GetObject"]
        Effect   = "Allow"
        Resource = "${var.input_bucket_arn}/*"
      },
      {
        Action   = ["dynamodb:PutItem"]
        Effect   = "Allow"
        Resource = aws_dynamodb_table.flagged_notices.arn
      },
      {
        Action   = ["ssm:GetParameter"]
        Effect   = "Allow"
        Resource = var.db_password_ssm_parameter_arn
      },
      {
        Action   = ["logs:CreateLogStream", "logs:PutLogEvents"]
        Effect   = "Allow"
        Resource = "${aws_cloudwatch_log_group.cluster.arn}:*"
      },
      {
        Action   = ["ecr:BatchGetImage*", "ecr:BatchCheck*", "ecr:Get*", "ecr:List*", "ecr:Describe*"]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role" "processing_task" {
  name                 = "${var.iam_role_prefix}_LAWFULNESS_PROCESSING_TASK_ROLE"
  assume_role_policy   = data.aws_iam_policy_document.ecs_assume_role_policy.json
  managed_policy_arns  = [aws_iam_policy.processing_task.arn]
  permissions_boundary = "arn:aws:iam::195848431569:policy/Team_Admin_Boundary"
}

resource "aws_security_group" "processing_task" {
  name        = "${var.resource_prefix}-processing-task"
  description = "Allow internet access in lawfulness processing task"
  vpc_id      = var.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ecs_task_definition" "processing_task" {
  family                   = "${var.resource_prefix}-processing-task"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.thread_count / 2 * 1024 # one physical core for 2 app threads should be enough
  memory                   = var.thread_count / 2 * 2048 # minimum amount of RAM allowed for the CPU value
  execution_role_arn       = aws_iam_role.processing_task.arn
  task_role_arn            = aws_iam_role.processing_task.arn
  container_definitions = jsonencode([
    {
      name               = "lawfulness"
      image              = var.task_image
      cpu                = var.thread_count / 2 * 1024 # one physical core for 2 app threads should be enough
      memory             = var.thread_count / 2 * 2048 # minimum amount of RAM allowed for the CPU value
      execution_role_arn = aws_iam_role.processing_task.arn
      task_role_arn      = aws_iam_role.processing_task.arn
      essential          = true
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.cluster.name
          awslogs-region        = var.region
          awslogs-stream-prefix = "ecs"
        }
      }
      environment = concat(local.task_environment, [{ name = "MODE", value = "task" }])
    }
  ])
}

resource "aws_scheduler_schedule" "processing_scheduler" {
  name                = "${var.resource_prefix}-processing-scheduler"
  group_name          = "default"
  schedule_expression = "cron(0 0 * * ? *)" # run every day at 00:00

  flexible_time_window {
    mode = "OFF"
  }

  target {
    arn      = aws_ecs_cluster.cluster.arn
    role_arn = aws_iam_role.processing_scheduler.arn

    ecs_parameters {
      task_definition_arn = aws_ecs_task_definition.processing_task.arn_without_revision
      launch_type         = "FARGATE"

      network_configuration {
        security_groups  = [aws_security_group.processing_task.id]
        subnets          = var.private_subnet_id_list
        assign_public_ip = false
      }
    }

    retry_policy {
      maximum_event_age_in_seconds = 300
      maximum_retry_attempts       = 10
    }
  }
}


data "aws_iam_policy_document" "scheduler_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["scheduler.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "processing_scheduler" {
  name = "${var.iam_policy_prefix}_LAWFULNESS_PROCESSING_SCHEDULER_POLICY"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["ecs:RunTask"]
        Effect   = "Allow"
        Resource = [aws_ecs_task_definition.processing_task.arn_without_revision]
      },
      {
        Action   = ["iam:PassRole"]
        Effect   = "Allow"
        Resource = [aws_iam_role.processing_task.arn]
      }
    ]
  })
}

resource "aws_iam_role" "processing_scheduler" {
  name                 = "${var.iam_role_prefix}_LAWFULNESS_PROCESSING_SCHEDULER_ROLE"
  assume_role_policy   = data.aws_iam_policy_document.scheduler_assume_role_policy.json
  managed_policy_arns  = [aws_iam_policy.processing_scheduler.arn]
  permissions_boundary = "arn:aws:iam::195848431569:policy/Team_Admin_Boundary"
}*/
