/*resource "aws_opensearchserverless_access_policy" "companies_fr" {
  name        = "companies-fr"
  type        = "data"
  description = "read and write permissions"
  policy = jsonencode([
    {
      Rules = [
        {
          ResourceType = "index",
          Resource = [
            "index/companies-fr/*"
          ],
          Permission = [
            "aoss:*"
          ]
        },
        {
          ResourceType = "collection",
          Resource = [
            "collection/companies-fr"
          ],
          Permission = [
            "aoss:*"
          ]
        }
      ],
      Principal = [
        "arn:aws:iam::${var.project_account_id}:user/DEV_TECHNICAL_USER",
        "arn:aws:sts::${var.project_account_id}:assumed-role/Team_Admin2/habragi"
      ]
    }
  ])
}

resource "aws_opensearchserverless_security_policy" "companies_fr_encryption" {
  name = "companies-fr"
  type = "encryption"
  policy = jsonencode({
    "Rules" = [
      {
        "Resource" = [
          "collection/companies-fr"
        ],
        "ResourceType" = "collection"
      }
    ],
    "AWSOwnedKey" = true
  })
}

resource "aws_opensearchserverless_security_policy" "companies_fr" {
  name        = "companies-fr"
  type        = "network"
  description = "VPC access"
  policy = jsonencode([
    {
      Description = "VPC access to collection and Dashboards endpoint for companies-fr collection",
      Rules = [
        {
          ResourceType = "collection",
          Resource = [
            "collection/companies-fr"
          ]
        },
        {
          ResourceType = "dashboard"
          Resource = [
            "collection/companies-fr"
          ]
        }
      ],
      AllowFromPublic = false,
      SourceVPCEs = [
        "vpce-01e042e9470ef3004"
      ]
    }
  ])
}

resource "aws_opensearchserverless_collection" "companies_fr" {
  name = "companies-fr"
  type = "SEARCH"

  depends_on = [aws_opensearchserverless_security_policy.companies_fr]
}*/
