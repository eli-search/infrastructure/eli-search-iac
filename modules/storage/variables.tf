variable "tags" {
  type = map(string)
}

variable "s3_input_bucket_name" {
  type = string
}

variable "s3_data_bucket_name" {
  type = string
}

variable "s3_curated_bucket_name" {
  type = string
}

variable "s3_ml_data_bucket_name" {
  type = string
}

variable "s3_ml_models_bucket_name" {
  type = string
}

variable "s3_backup_bucket_name" {
  type = string
}

variable "s3_deployment_bucket_name" {
  type = string
}

variable "ssm_buckets_input_id" {
  type = string
}

variable "ssm_buckets_curated_id" {
  type = string
}

variable "ssm_buckets_ml_data_id" {
  type = string
}

variable "ssm_buckets_backup_id" {
  type = string
}

variable "ssm_buckets_deployment_id" {
  type = string
}

variable "iam_policy_prefix" {
  type = string
}

variable "ssm_s3_input_bucket_readwrite_policy_arn" {
  type = string
}

variable "ssm_s3_curated_bucket_readwrite_policy_arn" {
  type = string
}

variable "ssm_s3_curated_bucket_readonly_policy_arn" {
  type = string
}

variable "ssm_s3_backup_bucket_readonly_policy_arn" {
  type = string
}

variable "ssm_s3_deployment_bucket_readwrite_policy_arn" {
  type = string
}

variable "ecr_repository_sagemaker_classifiers_name" {
  type = string
}

variable "ecr_repository_application_name" {
  type = string
}


variable "lawfulness_new_notices_queue_arn" {
  type = string
}