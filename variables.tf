# General
variable "tags" {
  type = map(string)
}

variable "project_account_id" {
  type = string
}

variable "region" {
  type = string
}

# Network
variable "vpc_id" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "private_subnet_id_list" {
  type = list(string)
}

variable "private_subnet_id_az1_list" {
  type = list(string)
}

variable "private_subnet_id_az2_list" {
  type = list(string)
}

# Terraform bootstrap
variable "terraform_s3_bucket_name" {
  type = string
}

variable "terraform_dynamodb_table_name" {
  type = string
}

# TED AI project
variable "s3_input_bucket_name" {
  type = string
}

variable "s3_data_bucket_name" {
  type = string
}

variable "s3_curated_bucket_name" {
  type = string
}

variable "s3_deployment_bucket_name" {
  type = string
}

variable "s3_ml_data_bucket_name" {
  type = string
}

variable "s3_ml_models_bucket_name" {
  type = string
}

variable "s3_backup_bucket_name" {
  type = string
}

variable "ecr_repository_sagemaker_classifiers_name" {
  type = string
}

variable "ecr_repository_application_name" {
  type = string
}

variable "applications_dashboard_repository_url" {
  type = string
}

variable "dashboard_port" {
  type = number
}

variable "sagemaker_classifiers_execution_role_name_prefix" {
  type = string
}

variable "sagemaker_classifier_multi_label_division_classifier_name" {
  type = string
}

variable "sagemaker_classifier_multi_label_division_classifier_image_url" {
  type = string
}

variable "sagemaker_classifier_multi_label_division_classifier_model_url" {
  type = string
}

variable "sagemaker_classifier_opentender_multi_label_division_classifier_name" {
  type = string
}

variable "sagemaker_classifier_opentender_multi_label_division_classifier_image_url" {
  type = string
}

variable "sagemaker_classifier_opentender_multi_label_division_classifier_model_url" {
  type = string
}

variable "sagemaker_classifier_budgetary_value_classifier_name" {
  type = string
}

variable "sagemaker_classifier_budgetary_value_classifier_image_url" {
  type = string
}

variable "sagemaker_classifier_budgetary_value_classifier_model_url" {
  type = string
}

# RDS

variable "db_identifier" {
  type = string
}

variable "db_password_secretsmanager_secret_name" {
  type = string
}

variable "db_name" {
  type = string
}

variable "db_instance_class" {
  type = string
}

variable "db_username" {
  type = string
}

# SSM
variable "ssm_prefix" {
  type = string
}

variable "ssm_buckets_input_id" {
  type = string
}

variable "ssm_buckets_curated_id" {
  type = string
}

variable "ssm_buckets_ml_data_id" {
  type = string
}

variable "ssm_buckets_backup_id" {
  type = string
}

variable "ssm_buckets_deployment_id" {
  type = string
}

variable "ssm_classifier_endpoint_multi_label_division_classifier_name" {
  type = string
}

variable "ssm_classifier_endpoint_opentender_multi_label_division_classifier_name" {
  type = string
}

variable "ssm_classifier_endpoint_budgetary_value_classifier_name" {
  type = string
}

variable "ssm_s3_input_bucket_readwrite_policy_arn" {
  type = string
}

variable "ssm_s3_curated_bucket_readwrite_policy_arn" {
  type = string
}

variable "ssm_s3_curated_bucket_readonly_policy_arn" {
  type = string
}

variable "ssm_s3_backup_bucket_readonly_policy_arn" {
  type = string
}

variable "ssm_s3_deployment_bucket_readwrite_policy_arn" {
  type = string
}

variable "ssm_cloudwatch_logs_write_policy_arn" {
  type = string
}

variable "ssm_ecs_ingest_resources_task_definition" {
  type = string
}

variable "ssm_ecs_ingest_resources_cluster_arn" {
  type = string
}

variable "ssm_ecs_ingest_resources_container_name" {
  type = string
}

variable "ssm_ecs_ingest_resources_security_group_id" {
  type = string
}

variable "ssm_ecs_notice_data_extraction_task_definition" {
  type = string
}

variable "ssm_ecs_notice_data_extraction_cluster_arn" {
  type = string
}

variable "ssm_ecs_notice_data_extraction_container_name" {
  type = string
}

variable "ssm_ecs_notice_data_extraction_security_group_id" {
  type = string
}

# Glue
variable "glue_database_name" {
  type = string
}

# IAM
variable "iam_role_prefix" {
  type = string
}

variable "iam_policy_prefix" {
  type = string
}

# Lawfulness
variable "lawfulness_resource_prefix" {
  type = string
}

variable "lawfulness_task_image" {
  type = string
}

variable "lawfulness_api_port" {
  type = number
}

variable "lawfulness_thread_count" {
  type = number
}

# Ingestion
variable "ingestion_resource_prefix" {
  type = string
}

variable "notice_ingestion_resources_url" {
  type = string
}

# Notice data extraction
variable "notice_data_extraction_prefix" {
  type = string
}

variable "notice_data_extraction_resources_url" {
  type = string
}